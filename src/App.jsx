import React, { useReducer } from 'react';
import { BrowserRouter, Route } from 'react-router-dom';
import Navbar from './components/Navbar';
import Context from "./utils/context";
import * as ACTIONS from './actionCreators/allActions';
import { productReducer } from "./reducers/product.reducer";
import { logoReducer } from "./reducers/logo.reducer";
import { userReducer } from "./reducers/user.reducer";
import { logo as defaultLogoState } from './defaultStates/logo';
import { products as defaultProductState } from './defaultStates/products';
import { user as defaultUserState } from './defaultStates/user';
import Logo from './components/Logo';
import AdminPart from './components/AdminPart';
import UserPart from './components/UserPart';
import Profile from './components/Profile';

function App() {
    const [stateLogo, dispatchLogoReducer] = useReducer(logoReducer, defaultLogoState);
    const [stateProduct, dispatchProductReducer] = useReducer(productReducer, defaultProductState);
    const [stateUser, dispatchUserReducer] = useReducer(userReducer, defaultUserState);

    const addProduct = (data) => {
        dispatchProductReducer(ACTIONS.addProduct(data));
    };

    const editProduct = (data) => {
        dispatchProductReducer(ACTIONS.editProduct(data));
    };

    const deleteProduct = (id) => {
        dispatchProductReducer(ACTIONS.deleteProduct(id));
    };

    const findProduct = (data) => {
        dispatchProductReducer(ACTIONS.findProduct(data));
    };

    const resetProducts = () => {
        dispatchProductReducer(ACTIONS.resetProducts());
    }

    const editUser = (data) => {
        dispatchUserReducer(ACTIONS.editUser(data));
    }

    return (
        <BrowserRouter>
            <Navbar />
            <Context.Provider
                value={{
                    logoState: stateLogo,
                    productState: stateProduct,
                    userState: stateUser,
                    addProduct, editProduct, deleteProduct, findProduct, resetProducts, editUser
                }}
            >
                <Route path='/home' component={Logo} />
                <Route path='/adminPart' component={AdminPart} />
                <Route path='/userPart' component={UserPart} />
                <Route path='/profile' component={Profile} />
            </Context.Provider>
        </BrowserRouter>
    )
}

export default App;