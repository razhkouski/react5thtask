import React from 'react';

const HandleError = (props) => {
    const textMessage = () => {
        if (props.length > 1 && props.length < 16) {
            return 'Указано неверное кол-во символов платёжной карты. Поддерживается только стандарт в 16 символов без пробелов.';
        } else if (props.length === 16) {
            return '✓'
        } else if (props.length > 16)
            return 'Указано неверное кол-во символов платёжной карты. Поддерживается только стандарт в 16 символов без пробелов (стандарты в 18 и более символов недоступны в текущей платёжной системе).'
    }

    return (
        <div>
            <p>{textMessage()}</p>
        </div>
    )
}

export default React.memo(HandleError, (prevProps, nextProps) => {
    if (nextProps.length > 14 && nextProps.length < 17) {
        return false;
    } else if (prevProps.length > 14 && prevProps.length < 17) {
        return false;
    }
    return true;
})