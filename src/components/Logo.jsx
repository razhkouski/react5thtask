import React, { useContext } from 'react';
import Context from '../utils/context';

function Logo(props) {
    const context = useContext(Context);
    
    return (
        <>
            <main key={context.logoState.id}>
                <div className="logoBlock">
                    <img src={context.logoState.img} alt='' />
                    <h1>{context.logoState.title}</h1>
                </div>
                <p>{context.logoState.description}</p>
            </main>
        </>
    )
}

export default Logo;