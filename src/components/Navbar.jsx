import React from 'react';
import { NavLink } from "react-router-dom";

function Navbar() {
    return (
        <nav>
            <div>
                <NavLink to='/home'>Logo</NavLink>
            </div>
            <div>
                <NavLink to='/adminPart'>Admin Part</NavLink>
            </div>
            <div>
                <NavLink to='/userPart'>User Part</NavLink>
            </div>
            <div>
                <NavLink to='/profile'>Profile</NavLink>
            </div>
        </nav>
    )
}

export default Navbar;