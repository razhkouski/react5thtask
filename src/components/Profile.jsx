import React, { useState, useContext } from 'react';
import Context from '../utils/context';
import ProfileInput from './ProfileInput';

const Profile = () => {
    const context = useContext(Context);
    const [name, setName] = useState('');
    const [surname, setSurname] = useState('');
    const [card, setCard] = useState('');

    const handleEdit = () => {
        let user = {
            name: name ? name : context.userState.name,
            surname: surname ? surname : context.userState.surname,
            card: card ? card : context.userState.card
        };

        context.editUser(user);

        setName('');
        setSurname('');
        setCard('');
    }

    return (
        <>
            <h1>User information</h1>
            <div key={context.userState.id}>
                <p>{context.userState.name} {context.userState.surname}</p>
                <p>Card number: {context.userState.card}</p>
            </div>
            Name: <ProfileInput data={name} setInputData={setName}></ProfileInput>
            Surname: <ProfileInput data={surname} setInputData={setSurname}></ProfileInput>
            Card number: <ProfileInput data={card} setInputData={setCard} flag={true}></ProfileInput>
            <button onClick={() => handleEdit(context.userState)}>Edit</button>
        </>
    )
};

export default Profile;