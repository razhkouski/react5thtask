import React from 'react';
import HandleError from './HandleError'

function ProfileInput(props) {
    return (
        <div>
            <input value={props.data} type="text" onChange={(e) => props.setInputData(e.target.value)} />
            {props.flag && <HandleError length={props.data.length} />}
        </div>
    )
}

export default ProfileInput;