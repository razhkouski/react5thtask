import React, { useState } from 'react';
import { useContext } from 'react';
import Context from '../utils/context';

const UserPart = () => {
    const context = useContext(Context);
    const [inputValue, setInputValue] = useState('');

    const handleSearch = () => {
        context.findProduct(inputValue);
        
        setInputValue('');
    };

    const handleReset = () => {
        context.resetProducts();
    }

    return (
        <div>
            <h1>Catalog</h1>
            <input value={inputValue} onChange={(e) => setInputValue(e.target.value)}></input>
            <button onClick={() => handleSearch()}>Find</button>
            <button onClick={() => handleReset()}>Reset</button>
            {
                context.productState.map(item => (
                    <div key={item.id}>
                        <p>{item.title}</p>
                        <img src={item.img} alt="" />
                        <p>{item.description}</p>
                        <p>{item.price} BYN</p>
                    </div>
                ))
            }
        </div>
    )
}

export default UserPart;